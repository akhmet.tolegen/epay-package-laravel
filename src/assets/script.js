jQuery(function($){

	var http = new XMLHttpRequest();
	var url = 'https://testoauth.homebank.kz/epay2/oauth2/token';

	//TODO collect this data dynamically from front and 
	var params = 'grant_type=' + grant_type + '&scope=' + scope + '&client_id=' + client_id + '&client_secret=' + client_secret + '&invoiceID=' + invoiceID + '&amount=' + amount + '&curency=' + currency + '&terminal=' + terminal;
	
	var backLink = "https://example.kz/success.html";
	var failureBackLink = "https://example.kz/failure.html";
	var postLink = "https://example.kz/";
	var failurePostLink = "https://example.kz/order/1123/fail";

	http.open('POST', url, true);

	http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=UTF-8');

	http.onreadystatechange = function() {//Call a function when the state changes.
		if(http.readyState == 4 && http.status == 200) {
			console.log(http.response);

			// Remove to another function with dynamic data such as auth(access_token), invoiceId(from shop(merchant)) and etc.
			var createPaymentObject = function(auth, invoiceId, amount) {
            var paymentObject = {
					invoiceId: invoiceId,
					backLink: backLink,
					failureBackLink: failureBackLink,
					postLink: postLink,
					failurePostLink: failurePostLink,
					language: "RU",
					description: "Оплата в интернет магазине",
					accountId: "testuser1",
					terminal: terminal,
					amount: amount,
					currency: currency
				};
				paymentObject.auth = auth;
				return paymentObject;
			};
			
			halyk.pay(createPaymentObject(http.response.access_token, invoiceID , amount));
		}
	}
	http.send(params);
 
});