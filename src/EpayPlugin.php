<?php
namespace plugin\hb_epay;
class EpayPlugin
{
    public function gateway($grant_type, $scope, $client_id, $client_secret, $invoiceID, $amount, $currency, $terminal)
    {
?>
        <script>
	    var grant_type = '<?php echo $grant_type ?>';
        var scope = '<?php echo $scope ?>';
        var client_id = '<?php echo $client_id ?>';
        var client_secret = '<?php echo $client_secret ?>';
        var invoiceID = '<?php echo $invoiceID ?>';
        var amount = '<?php echo $amount ?>';
        var currency = '<?php echo $currency ?>';
        var terminal = '<?php echo $terminal ?>';
        </script>

        <script src='https://test-epay.homebank.kz/payform/payment-api.js'></script>
        <script src='https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>
        <script src='script.js'></script>

    <?php
    }
}
?>