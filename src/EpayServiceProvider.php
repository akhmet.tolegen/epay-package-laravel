<?php
// packages/plugin/hb_epay/src/Greetr.php
namespace plugin\hb_epay;
use Illuminate\Support\ServiceProvider;

class EpayServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/assets' => public_path('/'),
        ], 'public');
    }

    public function register()
    {
        //
    }
}
